const personalMovieDB = {
    count: 0,
    movies: {},
    actors: {},
    genres: [],
    private: false,

    start: function () {
        let numberOfFilms = NaN;
        while ((numberOfFilms === '') || (numberOfFilms === null) || (isNaN(numberOfFilms))) {
            numberOfFilms = +prompt('Сколько фильмов вы уже посмотрели?', '');
        }
        return numberOfFilms;
    },

    rememberMyFilms: function () {

        let numberOfFilms = +prompt("Сколько фильмов вы уже посмотрели?");
        let control = Number(numberOfFilms);

        for (let i = 0; i < control; i++) {
            const a = prompt("Один из последних просмотренных фильмов?");
            const b = prompt("На сколько оцените его?");
            if ((a === null) || (b === null) || (a == '') || (b == '') || (a.length > 50)) {
                i--;
                console.log("Error");
            } else {
                personalMovieDB.movies[a] = b;
                console.log("Ok");
            }
        }

    },

    detectPersonalLevel: function () {
        if (personalMovieDB.count < 10) {
            alert("Просмотрено довольно мало фильмов");
        } else if (personalMovieDB.count <= 30) {
            alert("Вы классический зритель");
        } else if (personalMovieDB.count > 30) {
            alert("Вы киноман");
        } else {
            alert("Произошла ошибка");
        }
    },

    showMyDB: function () {
        if (personalMovieDB.privat == false) {
            console.log(personalMovieDB);
        }
    },

    toggleVisibleMyDb: function () {
        if (personalMovieDB.privat) {
            personalMovieDB.privat = false;
        }
        else {
            personalMovieDB.privat = true;
        }
    },


    /*В предыдущем задании №9 нашла ошибку... В данной функции перепутала названия genres и genre :(( */
    writeYourGenres: function () {
        for (let i = 1; i <= 3; i++) {
            let genre = prompt(`Ваш любимый жанр под номером ${i}?`);
            if ((genre == '') || (genre == null)) {
                console.log('Данные введены некорректно');
                i--;
            }
            else {
                personalMovieDB.genres[i - 1] = genre;
            }
        }
        personalMovieDB.genres.forEach((item, i) => {
            console.log(`Любимый жанр ${i + 1} - это ${item}`);
        });
    }
};


// personalMovieDB.start();
// personalMovieDB.detectPersonalLevel();
// personalMovieDB.showMyDB();
// personalMovieDB.toggleVisibleMyDB();

// personalMovieDB.writeYourGenres();
